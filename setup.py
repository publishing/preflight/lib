from setuptools import setup

setup(
    name='preflight',
    version='0.1',
    packages=['preflight', ],
    install_requires=['PyPDF2', 'Pillow']
)
